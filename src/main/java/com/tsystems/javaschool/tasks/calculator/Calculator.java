package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == "" || statement == null || statement.contains(","))
            return null;

        int length = statement.length();
        int open_br = statement.indexOf('(');
        if (open_br != -1) { //bracket handling
            int open_quan = 1;
            int close_quan = 0;
            int close_br = -1;
            for (int i = open_br + 1; i < length; i++) {
                if (statement.charAt(i) == '(') {
                    open_quan++;
                }
                if (statement.charAt(i) == ')') {
                    close_quan++;
                    if (close_quan == open_quan) {
                        close_br = i;
                        break;
                    }
                }
            }
            if (close_br == -1) return null;
            else {
                if (open_br == 0 && close_br == length - 1)
                    return evaluate(statement.substring(open_br, close_br));
                else if (open_br == 0 && close_br != length - 1) {
                    String subeval = evaluate(statement.substring(1, close_br));
                    return evaluate(subeval + statement.substring(close_br + 1));
                } else if (open_br != 0 && close_br == length - 1) {
                    String subeval = evaluate(statement.substring(open_br + 1, close_br));
                    return evaluate(statement.substring(0, open_br) + subeval);
                } else if (open_br == 1 || close_br == length - 2) return null;
                else {
                    String subeval = evaluate(statement.substring(open_br + 1, close_br));
                    return evaluate(statement.substring(0, open_br) + subeval + statement.substring(close_br + 1));
                }
            }
        }

        int sum = statement.indexOf('+');
        int substract = statement.indexOf('-');
        int multiply = statement.indexOf('*');
        int divide = statement.indexOf('/');

        String first_n, second_n = "";
        int first_dec = 0;
        int second_dec = 0;

        //multiplication
        if ((multiply < divide || divide == -1) && multiply != -1) {
            first_n = "";
            int start = -1;
            int finish = -1;
            for (int i = multiply - 1; i >= 0; i--) {
                if (statement.charAt(i) == '/' ||
                        statement.charAt(i) == '*' ||
                        statement.charAt(i) == '+' ||
                        (statement.charAt(i) == '-' && i != 0)
                        ) {
                    if (i == multiply - 1) return null;
                    else {
                        start = i + 1;
                        break;
                    }
                } else
                    first_n = statement.charAt(i) + first_n;
                if (statement.charAt(i) == '.') {
                    first_dec++;
                    if (first_dec > 1) return null;
                }
            }

            second_n = "";
            for (int i = multiply + 1; i < length; i++) {
                if (statement.charAt(i) == '/' ||
                        statement.charAt(i) == '*' ||
                        statement.charAt(i) == '+' ||
                        (statement.charAt(i) == '-' && i != multiply + 1)
                        ) {
                    if (i == multiply + 1) return null;
                    else {
                        finish = i - 1;
                        break;
                    }
                } else
                    second_n = second_n + statement.charAt(i);
                if (statement.charAt(i) == '.') {
                    second_dec++;
                    if (second_dec > 1) return null;
                }
            }

            if (first_n == "" || second_n == "") return null;

            if (first_dec != 0) {
                double first = Double.parseDouble(first_n);
                if (second_dec != 0) {
                    double second = Double.parseDouble(second_n);
                    double result = first * second;
                    return eval_result_double(statement, result, start, finish);
                } else {
                    int second = Integer.parseInt(second_n);
                    double result = first * second;
                    return eval_result_double(statement, result, start, finish);
                }
            } else {
                int first = Integer.parseInt(first_n);
                if (second_dec != 0) {
                    double second = Double.parseDouble(second_n);
                    double result = first * second;
                    return eval_result_double(statement, result, start, finish);
                } else {
                    int second = Integer.parseInt(second_n);
                    int result = first * second;
                    return eval_result_int(statement, result, start, finish);
                }
            }
        }

        //division
        if (divide != -1) {
            first_n = "";
            int start = -1;
            int finish = -1;
            for (int i = divide - 1; i >= 0; i--) {
                if (statement.charAt(i) == '/' ||
                        statement.charAt(i) == '*' ||
                        statement.charAt(i) == '+' ||
                        (statement.charAt(i) == '-' && i != 0)
                        ) {
                    if (i == divide - 1) return null;
                    else {
                        start = i + 1;
                        break;
                    }
                } else
                    first_n = statement.charAt(i) + first_n;
                if (statement.charAt(i) == '.') {
                    first_dec++;
                    if (first_dec > 1) return null;
                }
            }
            second_n = "";
            for (int i = divide + 1; i < length; i++) {
                if (statement.charAt(i) == '/' ||
                        statement.charAt(i) == '*' ||
                        statement.charAt(i) == '+' ||
                        (statement.charAt(i) == '-' && i != divide + 1)
                        ) {
                    if (i == divide + 1) return null;
                    else {
                        finish = i - 1;
                        break;
                    }
                } else
                    second_n = second_n + statement.charAt(i);
                if (statement.charAt(i) == '.') {
                    second_dec++;
                    if (second_dec > 1) return null;
                }
            }

            if (first_n == "" || second_n == "" || Integer.parseInt(second_n) == 0) return null;

            if (first_dec != 0) {
                double first = Double.parseDouble(first_n);
                if (second_dec != 0) {
                    double second = Double.parseDouble(second_n);
                    double result = first / second;
                    return eval_result_double(statement, result, start, finish);
                } else {
                    int second = Integer.parseInt(second_n);
                    double result = first / second;
                    return eval_result_double(statement, result, start, finish);
                }
            } else {
                int first = Integer.parseInt(first_n);
                if (second_dec != 0) {
                    double second = Double.parseDouble(second_n);
                    double result = first / second;
                    return eval_result_double(statement, result, start, finish);
                } else {
                    int second = Integer.parseInt(second_n);
                    double result = (double) first / second;
                    if ((double) first%second == 0)
                        return eval_result_int(statement, (int)result, start, finish);
                    else return eval_result_double(statement, result, start, finish);
                }
            }
        }

        //substraction
        if ((substract < sum || sum == -1) && substract != -1 && statement.charAt(0) != '-') {
            first_n = "";
            int start = -1;
            int finish = -1;
            for (int i = substract - 1; i >= 0; i--) {
                if (statement.charAt(i) == '/' ||
                        statement.charAt(i) == '*' ||
                        statement.charAt(i) == '+' ||
                        (statement.charAt(i) == '-')
                        ) {
                    if (i == substract - 1) return null;
                    else {
                        start = i + 1;
                        break;
                    }
                } else
                    first_n = statement.charAt(i) + first_n;
                if (statement.charAt(i) == '.') {
                    first_dec++;
                    if (first_dec > 1) return null;
                }
            }
            second_n = "";
            for (int i = substract + 1; i < length; i++) {
                if (statement.charAt(i) == '/' ||
                        statement.charAt(i) == '*' ||
                        statement.charAt(i) == '+' ||
                        (statement.charAt(i) == '-')
                        ) {
                    if (i == substract + 1) return null;
                    else {
                        finish = i - 1;
                        break;
                    }
                } else
                    second_n = second_n + statement.charAt(i);
                if (statement.charAt(i) == '.') {
                    second_dec++;
                    if (second_dec > 1) return null;
                }
            }

            if (first_n == "" || second_n == "") return null;

            if (first_dec != 0) {
                double first = Double.parseDouble(first_n);
                if (second_dec != 0) {
                    double second = Double.parseDouble(second_n);
                    double result = first - second;
                    return eval_result_double(statement, result, start, finish);
                } else {
                    int second = Integer.parseInt(second_n);
                    double result = first * second;
                    return eval_result_double(statement, result, start, finish);
                }
            } else {
                int first = Integer.parseInt(first_n);
                if (second_dec != 0) {
                    double second = Double.parseDouble(second_n);
                    double result = first - second;
                    return eval_result_double(statement, result, start, finish);
                } else {
                    int second = Integer.parseInt(second_n);
                    int result = first - second;
                    return eval_result_int(statement, result, start, finish);
                }
            }
        }

        //sum
        if (sum != -1) {
            first_n = "";
            int start = -1;
            int finish = -1;
            for (int i = sum - 1; i >= 0; i--) {
                if (statement.charAt(i) == '/' ||
                        statement.charAt(i) == '*' ||
                        statement.charAt(i) == '+') {
                    if (i == sum - 1) return null;
                    else {
                        start = i + 1;
                        break;
                    }
                } else
                    first_n = statement.charAt(i) + first_n;
                if (statement.charAt(i) == '.') {
                    first_dec++;
                    if (first_dec > 1) return null;
                }
            }
            second_n = "";
            for (int i = sum + 1; i < length; i++) {
                if (statement.charAt(i) == '/' ||
                        statement.charAt(i) == '*' ||
                        statement.charAt(i) == '+' ||
                        (statement.charAt(i) == '-')
                        ) {
                    if (i == sum + 1) return null;
                    else {
                        finish = i - 1;
                        break;
                    }
                } else
                    second_n = second_n + statement.charAt(i);
                if (statement.charAt(i) == '.') {
                    second_dec++;
                    if (second_dec > 1) return null;
                }
            }

            if (first_n == "" || second_n == "") return null;

            if (first_dec != 0) {
                double first = Double.parseDouble(first_n);
                if (second_dec != 0) {
                    double second = Double.parseDouble(second_n);
                    double result = first + second;
                    return eval_result_double(statement, result, start, finish);
                } else {
                    int second = Integer.parseInt(second_n);
                    double result = first + second;
                    return eval_result_double(statement, result, start, finish);
                }
            } else {
                int first = Integer.parseInt(first_n);
                if (second_dec != 0) {
                    double second = Double.parseDouble(second_n);
                    double result = first + second;
                    return eval_result_double(statement, result, start, finish);
                } else {
                    int second = Integer.parseInt(second_n);
                    int result = first + second;
                    return eval_result_int(statement, result, start, finish);
                }
            }
        }
        return "";
    }

    public String eval_result_double(String statement, double result, int start, int finish) {
        if (start == -1 && finish == -1) {
            result = result * 10000;
            int i = (int) Math.round(result);
            result = (double) i / 10000;
            return String.valueOf(result);
        } else if (start == -1 && finish != -1) {
            return evaluate(result + statement.substring(finish + 1));
        } else if (start != -1 && finish == -1) {
            return evaluate(statement.substring(0, start) + result);
        } else {
            return evaluate(statement.substring(0, start) + result + statement.substring(finish + 1));
        }
    }

    public String eval_result_int(String statement, int result, int start, int finish) {
        if (start == -1 && finish == -1) {
            return String.valueOf(result);
        } else if (start == -1 && finish != -1) {
            return evaluate(result + statement.substring(finish + 1));
        } else if (start != -1 && finish == -1) {
            return evaluate(statement.substring(0, start) + result);
        } else {
            return evaluate(statement.substring(0, start) + result + statement.substring(finish + 1));
        }
    }
}



package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        int size = inputNumbers.size();
        int height = 1;
        while (true) {
            size -= height;
            if (size == 0) {
                break;
            } else if (size < 0) {
                throw new CannotBuildPyramidException();
            }
            height++;
        }

        int num_ind = 0;
        int width = 2 * height - 1;
        Collections.sort(inputNumbers);

        int pyramid[][] = new int[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = width / 2 - i; j < width / 2 + i + 1; j += 2) {
                pyramid[i][j] = inputNumbers.get(num_ind);
                num_ind++;
            }
        }

        return pyramid;
    }


}
